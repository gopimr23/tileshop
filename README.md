# README #

This project is a eCommerce website for a ground Tile shop.

### What is this repository for? ###

* Ecoommerce website for tile business
* 0.0.1

### How do I get set up? ###

* Clone the repo
* install virtualbox and vagrant
* $ vagrant up 
* $ vagrant ssh
* npm run start


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Contribution guidelines ###

* Master - only hotfix and release branch 
* all features and bugfix are in wip/develop
* PR of master should have version bump

### Who do I talk to? ###

* Gopinath (gopimr2@gmail.com)
